import pytest
from Kmeans import kmeans
from sklearn import datasets

def test_kmeans():

    # iris数据集加载
    iris = datasets.load_iris()
    Y = kmeans(iris.data, 3)

    assert Y.shape == (150,)
    assert Y.dtype == int
    assert Y.min() == 0
    assert Y.max() == 2

    try:
        Y = kmeans(iris.data, -1)
    except ValueError as exc:
        assert exc.args[0] == "k must be positive"