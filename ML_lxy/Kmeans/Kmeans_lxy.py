import numpy as np

def kmeans(X:np.array, k:int, maxEpoch:int=100) -> np.array:
    '''
    :param X: 输入样本
    :param k: 分类数
    :param maxEpoch: 迭代次数
    :return Y: 类别
    '''
    point_num = X.shape[0]  # 样本数
    center_dx = np.random.choice(len(X), k, replace=False)  # 避免出现重复值

    center_points = X[center_dx]  # 随机选取k个中心点
    Y = np.zeros(point_num)  # 存放输出类别

    # 开始迭代
    for iter in range(maxEpoch):
        for i in range(point_num):
            # 计算每个样本点到中心点的距离
            # dis = [np.linalg.norm(X[i] - center_points[j]) for j in range(k)]
            dis = np.sqrt(np.sum(np.square(X[i] - center_points), 1))
            Y[i] = np.argmin(dis)

        # 更新中心点
        for i in range(k):
            center_points[i] = np.mean(Y[i]==i)

    return Y.astype(int)






