import numpy as np

def knn(X_train:np.array,y_train:np.array, X_test:np.array, k:int) -> np.array:
    '''
    :param X_train: 训练集
    :param y_train: 训练集标签
    :param X_test: 测试集
    :param k: 最近邻数目
    :return Y: 预测结果
    '''
    Y = []
    for i in range(X_test.shape[0]):
        dis = np.sqrt(np.sum(np.square(X_test[i] - X_train), 1))
        sorted_dx = np.argsort(dis)  # 升序排序后的原下标

        knearest_points = y_train[sorted_dx[:k]]
        Y.append(np.argmax(np.bincount(knearest_points)))
    return Y
