import pytest
from Knn import  knn

import numpy as np
from sklearn import datasets
from sklearn.model_selection import train_test_split

def test_knn():
    iris = datasets.load_iris()
    X = iris.data
    y = iris.target
    #以8:2划分训练集、测试集
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

    Y = knn(X_train, y_train,X_test,5)

    assert Y.shape == (30,)
    assert Y.dtype == int
    assert Y.min() == 0
    assert Y.max() == 2

    try:
        Y = knn(X_train, y_train,X_test,-1)
    except ValueError as exc:
        assert exc.args[0] == "k must be positive"
